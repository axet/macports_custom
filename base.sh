#!/bin/bash

CONF=$(echo "file://$PWD")
TARGET=/opt/local/etc/macports/sources.conf

install() {
  portindex || exit 1

  grep "$CONF" "$TARGET" > /dev/null && echo "repo already exist in $TARGET" && exit 0

  sudo sed -i "" "\|rsync://rsync.macports.org|i \\
  $CONF\\
  " "$TARGET" && echo "repo installed to $TARGET"
}

uninstall() {
  sudo sed -i "" "\|$CONF|d" "$TARGET" && echo "repo uninstalled $TARGET"
}
